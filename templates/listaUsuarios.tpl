{include 'header.tpl'}
{include 'barradenavegacion.tpl'}


<div class="row contenedorficha ">
  <table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">usuario</th>
        <th scope="col">Permisos de administrador</th>
        <th scope="col">Baneo</th>
      </tr>
    </thead>
    {foreach $usuarios item=usuario }
      <tbody>
        <tr>
          <th scope="row">
            <p>{$usuario->username}</p>
          </th>
          <td>
           {if ($usuario->privilegio== 2 )}
              <a type="button" class=" btn btn-warning " href="permiso/{$usuario->privilegio}/{$usuario->id_usuario}">
              <i class="fas fa-user fa-2x"></i> 
              Quitar permisos
              </a>
              {else}
                <a type="button" class="btn btn-primary" href="permiso/{$usuario->privilegio}/{$usuario->id_usuario}">
                  <i class="fa fa-diamond fa-2x" aria-hidden="true"></i>
                  Dar permisos
                </a>
           {/if}
          </td>
          <td> 
            <a type="button" href="expulsar/{$usuario->id_usuario}" class="btn btn-danger">Baneo Permanente</a>
          </td>
        </tr>
      </tbody>
    {/foreach}
  </table>
</div>


{include 'piedepagina.tpl'}