{literal}

<section id="app">

    <div  v-if="permiso>= 1" class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h4 class="mb-0">{{username}}</h4>
        </div>
        <div class= "justify-content-center col-md-12 align-items-center">
            <h6>Su comentario...</h4>
            <textarea name="Text1" cols="60" rows="5" id="textoComentario" class="col-md-10" ></textarea> 
        </div>
        <div class= "row justify-content-around align-items-center col-md-12 py-2" >
            <div>
                <button type="button" class="btn btn-danger"v-on:click="restar">-</button>
                <button type="button" class="btn btn-light" id="puntaje">{{puntaje}}</button>
                <button type="button" class="btn btn-success" v-on:click="sumar">+</button>
            </div>
            <button type="button" class="btn btn-primary" v-on:click="enviarComentario">Enviar</button>
        </div>
    </div>

    <div class="card">
              
        <div v-if="loading">Cargando...</div>
        
        <div v-if="!loading" v-for="comentario in comentarios" class="overflow-auto bg-secondary col-md-12 mt-3 pb-3 ">
            <div class="row col-md-12 justify-content-between align-items-center  py-2 " >
                <div class= "row  align-items-center px-4"> 
                    <h4>{{comentario.username}}</h4>
                    <div v-if="comentario.voto>=3"type="button" class="btn btn-success mx-1">+{{comentario.voto}}</div>
               
                    <div v-else type="button" class="btn btn-danger mx-1">+{{comentario.voto}}</div>
                    
                </div>
                <button v-if="permiso>1" v-on:click="eliminarComentario(comentario.comentario_id)" type="button" class="btn btn-danger" id="enviar"  ><i class="fas fa-trash"></i></button>
               
            </div>
            <div class="bg-light border border-primary rounded-lg col-md-11">
                <p>{{comentario.comentario}}
                </p>
            </div>        
        </div>
         
        

    </div>

</section>
{/literal}
