{include 'header.tpl'}
{include 'barradenavegacion.tpl'}


<div class="row contenedorficha ">
  <table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">Categoría</th>
        <th scope="col">Eliminar</th>
        <th scope="col">Modificar</th>
      </tr>
    </thead>
    {foreach $fichas item=ficha }
      <tbody>
        <tr>
          <th scope="row">
            <a href="filtrar/{$ficha->id_ficha}" class="">{$ficha->titulo}</a>
          </th>
          <td>
            <a type="button" href="eliminarcategoria/{$ficha->id_ficha}" class="btn btn-danger">Eliminar</a>
          </td>
          <td> 
            <a type="button" href="modificarcategoria/{$ficha->id_ficha}" class="btn btn-primary">Modificar</a>
          </td>
        </tr>
      </tbody>
    {/foreach}
  </table>
</div>


{include 'piedepagina.tpl'}