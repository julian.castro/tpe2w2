{include 'header.tpl'}
{include 'barradenavegacion.tpl'}
<div  class='row justify-content-center col-md-12 comentario' >
  <div  class='col-md-12'>
    <div class="contenedorjuego ">
      <div class="card juego ">
        <img src="{$juego->img}" class="card-img-top" alt="{$juego->titulo}">
        <div class="card-body">
          <h5 class="card-title ">{$juego->titulo}</h5>
          <p class="card-text">{$juego->descripcion}</p>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item">Categoria : {$juego->categoria_titulo} </li> 
          <li class="list-group-item">Placa de video: {$juego->so} </li>
          <li class="list-group-item">sistema operativo: {$juego->graficos}</li>
          <li class="list-group-item" value='5' id="jose">Espacio en disco: {$juego->espacio}</li>
        </ul>
        <div class="card-body">
          <a href="{$juego->link}" class="card-link">Descargar</a>
        </div>
      </div>
    </div>

  </div>
  <div class='col-md-8 center-block'>
    {include 'vue/enviarComentarios.tpl'}
  </div>
  
</div>
     
<script src="js/main.js"></script>



{include 'piedepagina.tpl'}