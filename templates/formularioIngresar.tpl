

    {include 'header.tpl'}
    {include 'barradenavegacion.tpl'}
<div class="formadministrador">

    <form action="verificar" method="Post">
        {if $error}
            <div class="alert alert-danger">{$mensaje}</div>
        {/if}
        
        <div class="form-group">
        <label for="exampleInputEmail1" class="textoform">Email </label>
        <input type="email" class="form-control" name="email"  id="exampleInputEmail1" aria-describedby="emailHelp">
        
        </div>
        <div class="form-group">
        <label for="exampleInputPassword1" class="textoform">Contraseña</label>
        <input type="password" name="contraseña" class="form-control" id="exampleInputPassword1">
        </div>
        
        <button type="submit" class="btn btn-primary" >Ingresar</button>
    </form>
</div>
