{include 'header.tpl'}
{include 'barradenavegacion.tpl'}
<div class="formadministrador">
    <form action="confirmarcambioscategoria/{$categoria->id_categoria}" method="GET">
    {if $error}
        <div class="alert alert-danger">{$mensaje}</div>
    {/if}
        <div class="form-group">
            <label for="exampleInputEmail1" class="textoform">Titulo de la Categoría: </label>
            <input type="text" class="form-control" name="categoria" value="{$categoria->titulo}" >
                    
        </div>                    
        <button type="submit" class="btn btn-primary" >Guardar cambios</button>
    </form>
</div>
{include 'piedepagina.tpl'}