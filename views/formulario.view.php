<?php
require_once('libs/smarty/Smarty.class.php');
require_once('constructor.view.php');

class FormularioVista extends ConstructorVista{

    public function imprimirformularioCargarJuego($error, $mensaje){
        
        $this->traerSmarty()->assign('error', $error);
        $this->traerSmarty()->assign('mensaje', $mensaje);
        $this->traerSmarty()->display('formularioJuego.tpl');         
    }

//******************************************************************************************* */

    public function imprimirformularioModificarJuego($ficha, $error, $mensaje){
        $this->traerSmarty()->assign('juego', $ficha);
        $this->traerSmarty()->assign('error', $error);
        $this->traerSmarty()->assign('mensaje', $mensaje);
        $this->traerSmarty()->display('formularioModificarJuego.tpl');         
    }

//******************************************************************************************* */
   
    public function imprimirFormularioCargarCategoria($error, $mensaje){
        $this->traerSmarty()->assign('error', $error);
        $this->traerSmarty()->assign('mensaje', $mensaje);
        $this->traerSmarty()->display('agregarcategoria.tpl');         
    }

//***************************************************************************************** */
    
    public function mostrarFicha($ficha,$error, $mensaje){
        $this->traerSmarty()->assign('juego', $ficha);
        $this->traerSmarty()->assign('error', $error);
        $this->traerSmarty()->assign('mensaje', $mensaje);
        $this->traerSmarty()->display('fichajuego.tpl');
    }

//***************************************************************************************** */
        
    public function imprimirFormularioIngresar($error, $mensaje){
        
        $this->traerSmarty()->assign('error', $error);
        $this->traerSmarty()->assign('mensaje', $mensaje);
        $this->traerSmarty()->display('formularioIngresar.tpl');
    }
//*******************************************************************************************/
    public function imprimirFormularioRegistro($error, $mensaje){
        $this->traerSmarty()->assign('error', $error);
        $this->traerSmarty()->assign('mensaje', $mensaje);
        $this->traerSmarty()->display('formularioRegistro.tpl');
    }

//**************************************************************************************** */

    public function imprimirFormularioModificarCategoria($categoria,$error, $mensaje){
        $this->traerSmarty()->assign('error', $error);
        $this->traerSmarty()->assign('mensaje', $mensaje);
        $this->traerSmarty()->assign('categoria', $categoria);
        $this->traerSmarty()->display('formularioModificarCategoria.tpl'); 
    }

//**************************************************************************************** */

    public function error(){
        $this->traerSmarty()->display('error404.tpl'); 
    }

}


  
