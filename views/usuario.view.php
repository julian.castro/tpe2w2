<?php
require_once('libs/smarty/Smarty.class.php');
require_once('constructor.view.php');

class VistaUsuario extends ConstructorVista{

    public function listarUsuarios($usuarios){
        $this->traerSmarty()->assign('usuarios', $usuarios);
        $this->traerSmarty()->display('listaUsuarios.tpl');
    }


}


  
