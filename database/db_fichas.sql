-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-07-2020 a las 19:40:20
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_fichas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL,
  `titulo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `titulo`) VALUES
(8, 'RPG'),
(10, 'MOBA'),
(11, 'SHOOTER'),
(12, 'RTS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `comentario_id` int(11) NOT NULL,
  `usuario` int(11) NOT NULL,
  `comentario` text NOT NULL,
  `juego` int(11) NOT NULL,
  `voto` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`comentario_id`, `usuario`, `comentario`, `juego`, `voto`) VALUES
(32, 1, 'muy buenoo', 43, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ficha`
--

CREATE TABLE `ficha` (
  `id_ficha` int(11) UNSIGNED NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `requisitos` varchar(200) NOT NULL,
  `img` varchar(60) DEFAULT NULL,
  `link` varchar(60) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ficha`
--

INSERT INTO `ficha` (`id_ficha`, `titulo`, `descripcion`, `requisitos`, `img`, `link`, `id_categoria`) VALUES
(40, 'Dota 2', 'Dota 2 es un juego multijugador de estrategia en tiempo real. Dos equipos de cinco jugadores tienen el objetivo de destruir las estructuras rivales controlando a personajes denominados héroes. Ellos no pueden controlar las estructuras ni a las unidades que aparecen cada cierto tiempo. Siempre se juega en un mismo mapa que tiene dos lados: Radiant y Dire, cada uno de ellos presenta ventajas y desventajas respecto a la visión, unidades neutrales y posicionamiento. Hay tres líneas donde chocan unidades no controladas: arriba, abajo y medio.', 'Windows 7 o  superior .Compatible con Linux/nVidia GeForce 8600 o 9600GT, ATI o AMD Radeon HD2600 o 3600/15 GB de espacio disponible', 'imagen/5f0347b1a4af37.36421196.jpg', 'https://store.steampowered.com/app/570/Dota_2/?l=spanish', 10),
(41, 'Counter Strike', 'Disfruta del juego de acción en línea n° 1 en el mundo. Sumérgete en el fragor de la guerra antiterrorista más realista con este archiconocido juego por equipos. Alíate con compañeros para superar misiones estratégicas, asalta bases enemigas, rescata rehenes, y recuerda que tu personaje contribuye al éxito del equipo y viceversa', 'Windows XP en adelante/tarjeta de video de 32 MB/300 Mb', 'imagen/5f0347f4553613.84771749.jpg', ' https://store.steampowered.com/app/10/CounterStrike/?l=span', 11),
(42, 'Age of Empires II', 'Uno de los mejores juegos de estrategia en tiempo real que aúna la emoción de \'Age Of Empires II: The Age of Kings\' y Age Of Empires II: The Conquerors Expansion\'.  Calidad, jugabilidad, interfaz de juego muy intuitiva y una gran diversión son sus señas.  Age Of Empires II nos permite elegir entre 13 civilizaciones diferentes, entre ellas están mongoles, celtas, vikingos o japoneses. Contiene cinco campañas basadas en figuras históricas como Juana de Arco, William Wallace o Gengis Khan.  La expansión The Conquerors añade nuevos escenarios, una campaña con el Cid Campeador y más civilizaciones como los mayas, los aztecas o los coreanos, además de mejoras en la interfaz, gráficos e inteligencia artificial.', 'Windows xp en adelante/placa de video de 32 mb/600 Mb', 'imagen/5f034816df26b8.62102689.jpg', 'https://mega.nz/file/TRIHARSD#KHeR92UKa0oN4ue_3tzugE4hbi0tLJ', 12),
(43, 'Paladins', 'Paladins es una curiosa mezcla de acción en primera persona de carácter competitivo y estrategia de manos de los creadores de Smite que, con formato free-to-play (gratuito), ofrece un interesante acercamiento con pinceladas de táctica y rol. Paladins: Champions of the Realm ofrece multitud de disparos por equipos, un gran sentido del humor y, sobre todo, una profunda personalización de personajes.', 'WINDOWS 10/Nvidia GeForce 8800 GT/30 Gb', 'imagen/5f03478b2837a7.30926303.jpg', '   https://store.steampowered.com/app/444090/Paladins/', 10),
(44, ' World of Warcraft', 'Juego de rol multijugador masivo online que nos permite introducirnos en el universo de la conocida saga de Blizzard que enfrenta a La Horda y La Alianza en una interminable lucha a través del reino de Azeroth. El juego nos permite elegir entre varias razas y clases que afectarán al sistema de juego.\r\n\r\nEl título ofrece incontables horas de juego en las que tendremos que ir superando misiones para aumentar el nivel de nuestro personaje y mejorar constantemente nuestro equipamiento en el proceso. Además de PvE, también tendremos la opción de enfrentarnos de forma directa otros jugadores en PvP, ya sea encontrándonos de forma fortuita con otros personajes mientras recorremos el mundo o en instancias específicamente diseñadas para tal propósito.\r\n\r\nEl contenido del juego es inabarcable, permitiendo aprender oficios de todo tipo, obtener mascotas, formar gremios o cabalgar en monturas. Aunque a día de hoy todas estas características pueden resultar totalmente estandarizadas dentro del género, lo cierto es que en Blizzard siempre han sabido darle un equilibrio a todas las mecánicas de juego. Millones de jugadores activos en todo el mundo son la mayor demostración del éxito y magnitismo de este videojuego.', '', 'imagen/warcraft.jpg', 'https://world-of-warcraft.uptodown.com/windows', 0),
(46, 'Warcraft', 'Juego de rol multijugador masivo online que nos permite introducirnos en el universo de la conocida saga de Blizzard que enfrenta a La Horda y La Alianza en una interminable lucha a través del reino de Azeroth. El juego nos permite elegir entre varias razas y clases que afectarán al sistema de juego.  El título ofrece incontables horas de juego en las que tendremos que ir superando misiones para aumentar el nivel de nuestro personaje y mejorar constantemente nuestro equipamiento en el proceso. Además de PvE, también tendremos la opción de enfrentarnos de forma directa otros jugadores en PvP, ya sea encontrándonos de forma fortuita con otros personajes mientras recorremos el mundo o en instancias específicamente diseñadas para tal propósito.  El contenido del juego es inabarcable, permitiendo aprender oficios de todo tipo, obtener mascotas, formar gremios o cabalgar en monturas. Aunque a día de hoy todas estas características pueden resultar totalmente estandarizadas dentro del género, lo cierto es que en Blizzard siempre han sabido darle un equilibrio a todas las mecánicas de juego. Millones de jugadores activos en todo el mundo son la mayor demostración del éxito y magnitismo de este videojuego.', 'Windows® 7  en adelante/NVIDIA® GeForce® GTX 470, ATI Radeon™ HD 5870 o superior/30 GB de espacio libre disponibles', 'imagen/5f034825dd0d00.31400788.jpg', 'https://world-of-warcraft.uptodown.com/windows', 12),
(47, 'Call of Duty', 'Call of Duty® ofrece el descarnado realismo y la intensidad cinematográfica de las épicas batallas de la II Guerra Mundial como nunca antes: a través de los ojos de ciudadanos soldado y héroes olvidados de la alianza de naciones que ayudaron a conformar el curso de la historia moderna. Adéntrate en el caos de la batalla como parte de un escuadrón bien entrenado, que despliega fuego de cobertura y pone a salvo a los heridos. Además de los movimientos y tácticas auténticos de un escuadrón, la personalidad y entrenamiento propios de cada soldado salen a la luz en el campo de batalla. No hay soldado o nación que ganara la guerra en solitario. Por primera vez, Call of Duty® capta la guerra desde distintas perspectivas: a través de los ojos de soldados americanos, británicos o rusos. Sumérgete en la batalla en 24 misiones que abarcan 4 campañas históricas interconectadas. Encárgate de objetivos de misión que van desde el sabotaje y el asalto total a misiones de sigilo, combate entre vehículos y rescate. Las armas, localizaciones, vehículos y sonidos de guerra auténticos contribuyen al realismo, sumergiéndote en la experiencia más intensa sobre la II Guerra Mundial hasta el momento.', 'Microsoft Windows 98-ME-2000-XP/(tarjeta de vídeo de 32 MB 100% compatible con DirectX 9.0b y Hardware T&L y los controladores más recientes/1,4 GB de espacio libre en el disco duro sin comprimir (más', 'imagen/5f03480913c5e8.88161880.jpg', 'https://store.steampowered.com/app/2620/Call_of_Duty/', 11),
(48, 'diablo', 'Diablo, el Señor del Terror, ha caído ante un valiente héroe en las profundas catacumbas de Tristán. Pero ahora que el héroe se ha marchado, un Nómada de la Oscuridad lo ha reemplazado y vaga por el mundo de Santuario, dejando sólo muerte y destrucción a su paso. Como héroe de la humanidad, debes enfrentarte a los servidores de los hermanos maléficos de Diablo y detener al Nómada de la Oscuridad antes de que lleve a cabo su terrible destino.', 'Windows® 7 - Windows® 8 - Windows® 10 64-bit (con el Service Pack más reciente)/Tarjeta gráfica DirectX con resolución de 800 x 600 — 1024 x 786 para Windows 8/1.75 GB de espacio en disco duro', 'imagen/5f0395c47adf17.01677362.jpg', 'https://www.malavida.com/es/soft/diablo-2/#gref', 8),
(49, 'Spore', 'Desde una sola Célula hasta un Dios Galáctico, haz que tu criatura evolucione en el universo de tus propias creaciones. Juega a lo largo de las cinco etapas evolutivas de Spore: Célula, Criatura, Tribu, Civilización, y Espacio. Cada etapa está caracterizada por sus propios estilos, retos, y objetivos. Puedes jugar como quieras ? empieza por una célula y cría una especie desde organismo rudimental de marisma a viajador intergaláctico, o empieza en el medio y crea tribus o civilizaciones en nuevos planetas. Lo que hagas con tu universo es tu decisión. Spore te da una multitud de instrumentos potentes pero fáciles de usar así puedes encargarte de todos los aspectos de tu universo: criaturas, vehículos, edificios, y hasta naves espaciales. Aunque Spore sea un juego de un solo jugador, tus creaciones serán compartidas automáticamente con otros jugadores por lo que habrá un número infinito de mundos para explorar y jugar.', 'Microsoft Windows® XP con Service Pack 1 y Vista/Tarjeta de vídeo de 128 MB con soporte para Pixel Shader./Al menos 4 GB de espacio en el disco duro, con al menos 1 GB de espacio adicional para las cr', 'imagen/5f03477b4d11a9.40859185.jpg', 'https://store.steampowered.com/app/17390/SPORE/', 12),
(51, ' LOL', 'Los jugadores disfrutan de la velocidad y la intensidad de la estrategia que deben desarrollar en tiempo real, con elementos de juegos de rol. El objetivo de los jugadores de \'Lol\' es proteger la base propia mientras en el mapa se localizan y destruyen las bases de los enemigos. Además, dependiendo del nivel tendrán que cumplir ciertos objetivos para poder causar daños al contrario. Es posible  comprar objetos durante la partida que puedan ayudar a completarla misión e ,incluso, es posible comprar personajes. Sin embargo, también se puede jugar con un sistema de niveles para ir mejorando las habilidades de un personaje en lugar de adquirirlas en la tienda.', 'Windows XP (solo Service Pack 2 o 3), Windows Vista o Windows 7./Tarjeta gráfica compatible con Shader 2.0./8 GB de espacio libre en el disco duro.', 'imagen/5f0347e7ce2ef6.08866790.jpg', 'https://signup.las.leagueoflegends.com/es/signup/index#/', 10),
(52, 'The Witcher', 'The Witcher: Wild Hunt es un juego de rol de mundo abierto de nueva generación con una apasionante trama, ambientado en un espectacular universo de fantasía lleno de decisiones trascendentales y consecuencias impactantes. En The Witcher encarnas a Geralt de Rivia, un cazador de monstruos profesional que tiene que encontrar a la muchacha protagonista de una profecía en un amplio mundo abierto y rebosante de ciudades comerciales, islas con piratas vikingos, peligrosos puertos de montaña y cuevas olvidadas.', '64-bit Windows 7 or 64-bit Windows 8 (8.1)/Nvidia GPU GeForce GTX 660 - AMD GPU Radeon HD 7870/35 GB ', 'imagen/5f03475a565045.23396597.jpg', 'https://www.gamestorrents.nu/juegos-pc/the-witcher-3-wild-hu', 8),
(53, 'GTA V', 'Un joven estafador callejero, un ladrón de bancos retirado y un psicópata aterrador se meten en un lío, y tendrán que llevar a cabo una serie de peligrosos golpes para sobrevivir en una ciudad en la que no pueden confiar en nadie, y mucho menos los unos en los otros.', 'Windows 10 de 64 bits, Windows 8.1 de 64 bits, Windows 8 de 64 bits, Windows 7 de 64 bits con Service Pack 1/NVIDIA 9800 GT de 1 GB - AMD HD 4870 de 1 GB (DX 10, 10.1, 11)/90 GB available space', 'imagen/5f04b2a4890fb3.27615202.jpg', 'https://www.epicgames.com/store/es-ES/product/grand-theft-au', 11),
(57, '', '', '//', NULL, '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `email` varchar(40) NOT NULL,
  `contraseña` varchar(72) NOT NULL,
  `username` varchar(55) NOT NULL,
  `privilegio` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `email`, `contraseña`, `username`, `privilegio`) VALUES
(1, 'juanadministrador@admin.com', '$2a$10$nZ6un6j2/lv.rKoo/qfj5OGmcfZubaeq7qTopihot64zITp7xg.HS', 'juan', 2),
(7, 'jose@jose', '$2y$10$SBmOK9B0gnPJVZQgfDZREeQZC3YHWjT3P5VPsdC1XZ1WJjieWsUuu', 'jose', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_categoria`),
  ADD UNIQUE KEY `id_categoria` (`id_categoria`);

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`comentario_id`),
  ADD KEY `juego` (`juego`),
  ADD KEY `usuario` (`usuario`);

--
-- Indices de la tabla `ficha`
--
ALTER TABLE `ficha`
  ADD PRIMARY KEY (`id_ficha`),
  ADD UNIQUE KEY `id_ficha` (`id_ficha`),
  ADD KEY `id_categoria` (`id_categoria`),
  ADD KEY `id_categoria_2` (`id_categoria`),
  ADD KEY `id_categoria_3` (`id_categoria`),
  ADD KEY `id_categoria_4` (`id_categoria`),
  ADD KEY `id_categoria_5` (`id_categoria`),
  ADD KEY `id_categoria_6` (`id_categoria`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `comentario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `ficha`
--
ALTER TABLE `ficha`
  MODIFY `id_ficha` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `comentarios_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`id_usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
