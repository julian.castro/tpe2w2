<?php
require_once 'proyecto.controler.php';
require_once 'models/categoria.model.php';
require_once 'models/ficha.model.php';//enlazo con archivo de modelo.
require_once 'views/ficha.view.php';//enlazo con archivo de vista.

class ControladorCategoria extends ControladorPadre {

    
    
    public function subirCategoria(){
        $nivel=1;
        $this->VerificarRegistro( $nivel);
        $categoria = $_GET['categoria'];
        if($categoria==''){
            $error=1;
            $mensaje='Falta llenar un campo';
            $this->traerVistaFormulario()->imprimirFormularioCargarCategoria($error, $mensaje);    
        }else{
             $this->traerModeloFichas()-> insertarCategoria($categoria);
            header("Location: " . BASE_URL . "listadoCategorias/todos");
        }
       
    }

//----------------------------------------------------------------------
    
    public function listadoCategorias( $opcion){
        if ($opcion=="todos"){
            $nivel=1;
            $this->VerificarRegistro( $nivel);
            $this->traerVistaFicha()->tablaCategorias();

        }else{
            $juegos = $this->traerModeloFichas()->traerFichasPorCategoria($opcion);
            $this->traerVistaFicha()->mostrarGaleria($juegos);
        }
           
    }

//-----------------------------------------------------------------------------------
    //router94/categoriacontroller34/categoriamodelo30
    public function eliminarCategoria($id_categoria) {
        $nivel=1;
        $this->VerificarRegistro( $nivel);
        $this->traerModeloCategoria()->eliminarCategoria($id_categoria);
        header("Location: " . BASE_URL . "listadoCategorias/todos");
    }
//-----------------------------------------------------------------------------------
    public function confirmarCambiosCategoria($categoria){
        $nivel=1;
        $this->VerificarRegistro( $nivel);
        $nombre=$this->traerModeloCategoria()->traerCategoria($categoria);
        $titulo = $_GET['categoria'];
        if($titulo==''){
            $error=1;
            $mensaje='Falta llenar un campo';
            $this->traerVistaFormulario()->imprimirFormularioModificarCategoria($nombre,$error, $mensaje);    
        }else{
            $this->traerModeloCategoria()-> modificarCategoria($titulo, $categoria);
            header("Location: " . BASE_URL . "listadoCategorias/todos");
        }

        
    }
    public function error(){
        $this->traerVistaFicha()->error();
    }
}

