<?php

class ControladorFormulario extends ControladorPadre {


    public function formularioCargarJuego(){
        $nivel=1;
        $this->VerificarRegistro( $nivel);
        $error=0;
        $mensaje=0;
        $this->traerVistaFormulario()->imprimirFormularioCargarJuego($error, $mensaje);
    }
    public function formularioModificarJuego($id_juego){
        $nivel=1;
        $this->VerificarRegistro( $nivel);
        $ficha=$this->traerModeloFichas()->traerFicha($id_juego);
        $error=0;
        $mensaje=0;
        $this->traerVistaFormulario()->imprimirFormularioModificarJuego($ficha,$error, $mensaje);
    }

    public function formularioIngresar(){
        $error=0;
        $mensaje=0;
        $this->traerVistaFormulario()->imprimirFormularioIngresar($error, $mensaje);
    }
    public function formularioRegistro(){
        $error=0;
        $mensaje=0;
        $this->traerVistaFormulario()->imprimirFormularioRegistro($error, $mensaje);
    }

    public function formularioCargarCategoria(){
        $nivel=1;
        $this->VerificarRegistro( $nivel);
        $error=0;
        $mensaje=0;
        $this->traerVistaFormulario()->imprimirFormularioCargarCategoria($error, $mensaje);    
    }

    public function formularioModificarCategoria($id_categoria){
        $nivel=1;
        $this->VerificarRegistro( $nivel);
        $categoria=$this->traerModeloCategoria()->traerCategoria($id_categoria);
        $error=0;
        $mensaje=0;
        $this->traerVistaFormulario()->imprimirFormularioModificarCategoria($categoria,$error, $mensaje);    
    }
    function error(){
        $this->traerVistaFormulario()->error();
    }

}