<?php
require_once 'proyecto.controler.php';
require_once 'models/categoria.model.php';
require_once 'models/ficha.model.php';//enlazo con archivo de modelo.
require_once 'views/ficha.view.php';//enlazo con archivo de vista.

class ControladorFicha extends ControladorPadre {

    
    public function subirFicha() {
        $nivel=1;
        $this->VerificarRegistro( $nivel);

        $titulo = $_POST['titulo'];
        $descripcion = $_POST['descripcion'];
        $so = $_POST['so'];
        $graficos = $_POST['grafico'];
        $espacio =$_POST['espacio'];
        $link = $_POST['link'];
        $categoria = $_POST['categoria'];
        $requisitos= $this->requisitos($so, $graficos, $espacio);
        // inserta en la DB y redirige
       
         
        if ($titulo=='' || $descripcion==''){
            $error=1;
            $mensaje='llenar titulo y descrpcion';
            $this->traerVistaFormulario()->imprimirFormularioCargarJuego($error, $mensaje);
        }else{
            if (($_FILES['imput']['type'] == "image/jpg" || 
            $_FILES['imput']['type'] == "image/jpeg" || 
            $_FILES['imput']['type'] == "image/png")) {
                $success =  $this->traerModeloFichas()->insertConImagen($titulo, $descripcion, $categoria, $requisitos, $link);
                header("Location: " . BASE_URL . "");
            } else {
                $success = $this->traerModeloFichas()-> insertarFicha($titulo, $descripcion, $categoria, $requisitos, $link);
            header("Location: " . BASE_URL . "");
            }        
        }
    }

//router20/fichacontroller/33/fichamodel43-----------------------------------------------------------------------------------
//router20/fichacontroller/33/fichaview14-----------------------------------------------------------------------------------

    public function listarFichas() {
        // pido todos los productos al MODELO
        $fichas = $this->traerModeloFichas()->traerTodasFichas();
        // actualizo la vista
         $this->traerVistaFicha()->mostrarGaleria($fichas);
    }
//router26/fichacontroller40/
    public function listadoJuegos( $juego){
        if ($juego=="todos"){
            $fichas = $this->traerModeloFichas()->traerTodasFichas();
            $this->traerVistaFicha()->tablaJuegos($fichas);
        }
    }

//-----------------------------------------------------------------------------------

    public function mostrarJuego($juego){
        $ficha=$this->traerModeloFichas()->traerFicha($juego);
        $this->traerVistaFicha()->mostrarFicha($ficha);
    }
    private function requisitos ($so, $graficos, $espacio){
        $requisito= $so . '/' . $graficos . '/' .$espacio ;

        return $requisito;

    }
    public function eliminarJuego($id_juego) {
        $nivel=1;
        $this->VerificarRegistro( $nivel);
        $this->traerModeloFichas()->eliminarJuego($id_juego);
        header("Location: " . BASE_URL . "listadojuegos/todos");
    }
         
//-----------------------------------------------------------------------------------

    public function confirmarcambiosjuegos($ficha){
        $nivel=1;
        $this->VerificarRegistro( $nivel);
        $titulo = $_POST['titulo'];
        $descripcion = $_POST['descripcion'];
        $so = $_POST['so'];
        $graficos = $_POST['grafico'];
        $espacio =$_POST['espacio'];
        $link = $_POST['link'];
        $categoria = $_POST['categoria'];

        $requisitos= $this->requisitos($so, $graficos, $espacio);
        
        if ($titulo=='' || $descripcion==''){
            $error=1;
            $mensaje='llenar titulo y descrpcion';
            $juego=$this->traerModeloFichas()->traerFicha($ficha);
            $this->traerVistaFormulario()->imprimirFormularioModificarJuego($juego,$error, $mensaje);
        }else{
            if (($_FILES['imput']['type'] == "image/jpg"  ||
                $_FILES['imput']['type'] == "image/jpeg" || 
                $_FILES['imput']['type'] == "image/png")) {
                    $success =  $this->traerModeloFichas()->modificarConImagen($titulo, $descripcion, $categoria, $requisitos, $link, $ficha);
                    header("Location: " . BASE_URL . "");
            } else {
                $success =$this->traerModeloFichas()-> modificarJuego($titulo, $descripcion, $categoria, $requisitos, $link, $ficha);
                header("Location: " . BASE_URL . "listadojuegos/todos");
            header("Location: " . BASE_URL . "");
            }    
        }        
    }
}

