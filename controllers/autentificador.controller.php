<?php

require_once 'models/usuario.model.php';
require_once 'proyecto.controler.php';
require_once 'models/categoria.model.php';
require_once 'models/ficha.model.php';//enlazo con archivo de modelo.
require_once 'views/ficha.view.php';//enlazo con archivo de vista.

class ControladorAutentificador extends ControladorPadre {

    
    public function verificar() {
        $email = $_POST['email'];
        $contraseña = $_POST['contraseña'];

        // busco el usuario
        $usuario = $this->traerModeloUsuario()->traerUsuario($email);

        // si existe el usuario y el password es correcto
        if ($usuario && password_verify($contraseña, $usuario->contraseña)) { 

            // abro session y guardo al usuario logueado
            session_start();
            $_SESSION['registrado'] = true;
            $_SESSION['id_usuario'] = $usuario->id_usuario;
            $_SESSION['email'] = $usuario->email;
            $_SESSION['username'] = $usuario->username;
            $_SESSION['privilegio'] = $usuario->privilegio;
            
            header("Location: " . BASE_URL . "");
        } else {
            $error=0;
            $mensaje='fallo ingreso, verifique sus datos y intente de nuevo';
            $this->traerVistaFormulario()->imprimirFormularioIngresar($error, $mensaje);
        }

    }
    public function registro() {
       
        $email = $_POST['email'];
        $contraseña = $_POST['contraseña'];
        $username = $_POST['nombre'];
        
        $hash = password_hash($contraseña, PASSWORD_DEFAULT);
        
        $usuario = $this->traerModeloUsuario()->traerUsuario($email);

        if ($usuario) {
            $error=0;
            $mensaje='el usuario ya exite';
            $this->traerVistaFormulario()->imprimirFormularioRegistro($error, $mensaje);
        } else {
            $this->traerModeloUsuario()->registrarUsuario($email, $hash, $username );
            $this->verificar();
        }
    }
    public function listarUsuarios (){
        $nivel=2;
        $this->VerificarRegistro( $nivel);
        $usuarios= $this->traerModeloUsuario()->traerTablaUsuarios( $nivel);
        $this->traerVistaUsuario()->listarUsuarios($usuarios);
    }
    public function cerrarSesion() {
        session_start();
        session_destroy();
        header("Location: " . BASE_URL . '');
    }
    public function administrarPermisos($permiso, $usuario) {
        $nivel=2;
        $this->VerificarRegistro( $nivel);
        $nuevoPermiso=0;
        if ($permiso==2){
            $nuevoPermiso=1;
        }else {
            $nuevoPermiso=2;
        }
        $this->traerModeloUsuario()->cambiarPermiso($nuevoPermiso, $usuario);
        header("Location: " . BASE_URL . 'usuarios/permisos');
    }
    public function eliminarUsuario($id_user){
        $nivel=2;
        $this->VerificarRegistro( $nivel);
        $this->traerModeloUsuario()->eliminarUsuario($id_user);
        header("Location: " . BASE_URL . "usuarios");
    }
}