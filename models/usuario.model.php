<?php

class ModeloUsuario {

    private $db;

    public function __construct() {
        $this->db = $this->crearConexion();
    }

     /**
     * Crea la conexion
     */
    private function crearConexion() {
        //listo en variables los datos para abrir la coneccion.
        $host = 'localhost';
        $nombreDeUsuario = 'root';
        $contraseña = '';
        $baseDeDatos = 'db_fichas';
        try {
        $pdo = new PDO("mysql:host=$host;dbname=$baseDeDatos;charset=utf8", $nombreDeUsuario, $contraseña);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $pdo;
    }
/***********************************************************************************************************************/
    public function traerUsuario($email) {
        $sentencia = $this->db->prepare("SELECT * FROM usuarios WHERE email = ?");
        $sentencia->execute([$email]);
        return $sentencia->fetch(PDO::FETCH_OBJ);
    }
/***********************************************************************************************************************/
    public function registrarUsuario($email, $contraseña, $username){
        $db = $this->crearConexion();
        $privilegioDefault= 1;
        // 2. enviamos la consulta
        $sentencia = $db->prepare("INSERT INTO usuarios(email, contraseña, username, privilegio)
        VALUES(?, ?, ?, ?)"); // prepara la consulta
        $sentencia->execute([$email, $contraseña, $username, $privilegioDefault]); // ejecuta
    }
/************************************************************************************************************************/
    public function traerTablaUsuarios(){
        $sentencia = $this->db->prepare("SELECT * FROM usuarios");
        $sentencia->execute([]);
        return $sentencia->fetchAll(PDO::FETCH_OBJ);

    }
    public function cambiarPermiso($permiso, $usuario){
        // 2. enviamos la consulta
        $sentencia = $this->db->prepare("UPDATE usuarios SET privilegio = ? WHERE usuarios.id_usuario = ? "); // prepara la consulta
        $sentencia->execute([$permiso, $usuario]); // ejecuta
    }
    public function eliminarUsuario($id_user){
        $db = $this->crearConexion();
        // 2. enviamos la consulta
        $sentencia = $db->prepare("DELETE FROM usuarios WHERE id_usuario = ?"); // prepara la consulta
        $sentencia->execute([$id_user]); // ejecuta    
    }
}