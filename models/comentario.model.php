<?php

class ModeloComentarios {

    private function crearConexion() {
        //listo en variables los datos para abrir la coneccion.
        $host = 'localhost';
        $nombreDeUsuario = 'root';
        $contraseña = '';
        $baseDeDatos = 'db_fichas';
        try {
        $pdo = new PDO("mysql:host=$host;dbname=$baseDeDatos;charset=utf8", $nombreDeUsuario, $contraseña);
        } catch (Exception $e) {
            
        }
        return $pdo;
    }
/*************************************************************************************************************************/
    public function traerComentarios($juego){
        $db = $this->crearConexion();
        $sql="SELECT comentarios.*, usuarios.username as username FROM comentarios, usuarios WHERE (comentarios.usuario=usuarios.id_usuario AND comentarios.juego = ?)";
        $sentencia = $db->prepare($sql);
        $sentencia->execute([$juego]);
        $comentarios = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $comentarios;

    }
/***************************************************************************************************************************/
    public function insertarComentario($usuario, $juego, $texto, $voto){
        $exist= $this->traerComentariosUserYJuego($usuario, $juego);
        if ($exist){
            $this->modificarComentario($exist->comentario_id,$texto,$voto);
        }else{
             $db = $this->crearConexion();
            $sentencia = $db->prepare("INSERT INTO comentarios(usuario, comentario, juego, voto)
            VALUES(?, ?, ?, ?)"); // prepara la consulta
            $sentencia->execute([$usuario, $texto, $juego, $voto]); // ejecuta
        }
       
    }
/***************************************************************************************************************************/
    public function eliminarComentario($comentario){
        try {
            error_reporting(0);
            $db = $this->crearConexion();
            // 2. enviamos la consulta
            $sentencia = $db->prepare("DELETE FROM comentarios WHERE comentario_id = ?"); // prepara la consulta
            $sentencia->execute([$comentario]);
            return true;
        }
        catch(Exception $e){
            return false;
        } // ejecuta 
    }
    public function traerComentariosUserYJuego($user, $juego){
        $db = $this->crearConexion();
        $sql="SELECT * FROM comentarios WHERE (comentarios.usuario= ? AND comentarios.juego = ?)";
        $sentencia = $db->prepare($sql);
        $sentencia->execute([$user ,$juego]);
        $comentarios = $sentencia->fetch(PDO::FETCH_OBJ);
        return $comentarios;
    }
    public function modificarComentario($idComentario,$comentario,$voto){
        $db = $this->crearConexion();
        // 2. enviamos la consulta
        $sentencia = $db->prepare("UPDATE comentarios SET  comentario= ? , voto= ? WHERE comentarios.comentario_id = ? "); // prepara la consulta
        $sentencia->execute([$comentario, $voto, $idComentario]); // ejecuta
    }
}