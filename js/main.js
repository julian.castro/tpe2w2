"use strict"


let app = new Vue({
    el: "#app",
    data: {
        loading: false,
        comentarios: [],
        id: 0,
        id_usuario: 0,
        permiso: 0,
        username: '',
        puntaje: 5,

    },
    methods: {
        eliminarComentario: function(comentario) {
            let url = 'api/comentario/' + comentario;

            fetch(url, {
                method: 'DELETE',
            })
            traerComentariosAPI();
        },
        enviarComentario: function(e) {
            enviarComentario();
        },
        sumar: function(e) {
            sumar();
        },
        restar: function(e) {
            restar();
        }
    }
});
setInterval(traerComentariosAPI, 3000);
tomarDatos();
tomaridJuego()
traerComentariosAPI();




function sumar() {
    if (app.puntaje < 5) {
        app.puntaje++;
    }
}

function restar() {
    if (app.puntaje > 1) {
        app.puntaje--;
    }
}

function tomaridJuego() {
    let urlActual = window.location.pathname;
    let dividirCadena = urlActual.split('/');
    app.id = dividirCadena[4];
}

function tomarDatos() {
    let id = document.getElementById("dato");
    let nombre = document.getElementById("dato2");
    let permiso = document.getElementById("dato3");

    app.id_usuario = id.value;
    app.username = nombre.value;
    app.permiso = permiso.value;

}

function traerComentariosAPI() {
    app.loading = true;
    fetch("api/comentario/traer/" + app.id)
        .then(response => response.json())
        .then(json => {
            // asigno las tareas que me devuelve la API
            app.comentarios = json; // es como el $this->smarty->assign("tasks", tareas);
            app.loading = false;
        });
}


function enviarComentario() {
    let texto = document.getElementById("textoComentario").value;
    let puntaje = app.puntaje;
    if (texto == '' || (puntaje < 1 & puntaje > 5)) {
        alert("debe realizar un comentario y un puntaje valido");
    } else {
        let data = {
            "texto": texto,
            "puntaje": puntaje,
            "id_usuario": app.id_usuario,
            "juego": app.id,
        }

        fetch('api/comentario', {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        })
        traerComentariosAPI();
    }
}