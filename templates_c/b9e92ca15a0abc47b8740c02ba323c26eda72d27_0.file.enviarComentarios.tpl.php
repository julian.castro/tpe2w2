<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-07 18:21:28
  from 'C:\xampp\htdocs\servidor\tpe2w2\templates\vue\enviarComentarios.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f04a108783938_38443438',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b9e92ca15a0abc47b8740c02ba323c26eda72d27' => 
    array (
      0 => 'C:\\xampp\\htdocs\\servidor\\tpe2w2\\templates\\vue\\enviarComentarios.tpl',
      1 => 1594138872,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f04a108783938_38443438 (Smarty_Internal_Template $_smarty_tpl) {
?>

<section id="app">

    <div  v-if="permiso>= 1" class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h4 class="mb-0">{{username}}</h4>
        </div>
        <div class= "justify-content-center col-md-12 align-items-center">
            <h6>Su comentario...</h4>
            <textarea name="Text1" cols="60" rows="5" id="textoComentario" class="col-md-10" ></textarea> 
        </div>
        <div class= "row justify-content-around align-items-center col-md-12 py-2" >
            <div>
                <button type="button" class="btn btn-danger"v-on:click="restar">-</button>
                <button type="button" class="btn btn-light" id="puntaje">{{puntaje}}</button>
                <button type="button" class="btn btn-success" v-on:click="sumar">+</button>
            </div>
            <button type="button" class="btn btn-primary" v-on:click="enviarComentario">Enviar</button>
        </div>
    </div>

    <div class="card">
              
        <div v-if="loading">Cargando...</div>
        
        <div v-if="!loading" v-for="comentario in comentarios" class="overflow-auto bg-secondary col-md-12 mt-3 pb-3 ">
            <div class="row col-md-12 justify-content-between align-items-center  py-2 " >
                <div class= "row  align-items-center px-4"> 
                    <h4>{{comentario.username}}</h4>
                    <div v-if="comentario.voto>=3"type="button" class="btn btn-success mx-1">+{{comentario.voto}}</div>
               
                    <div v-else type="button" class="btn btn-danger mx-1">+{{comentario.voto}}</div>
                    
                </div>
                <button v-if="permiso>1" v-on:click="eliminarComentario(comentario.comentario_id)" type="button" class="btn btn-danger" id="enviar"  ><i class="fas fa-trash"></i></button>
               
            </div>
            <div class="bg-light border border-primary rounded-lg col-md-11">
                <p>{{comentario.comentario}}
                </p>
            </div>        
        </div>
         
        

    </div>

</section>

<?php }
}
