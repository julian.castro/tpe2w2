<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-07 14:28:26
  from 'C:\xampp\htdocs\servidor\tpe2w2\templates\listaUsuarios.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f046a6a19adb2_36163827',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '55dd6fb805da954af5e2bb1808e5d58c67a90c12' => 
    array (
      0 => 'C:\\xampp\\htdocs\\servidor\\tpe2w2\\templates\\listaUsuarios.tpl',
      1 => 1594124877,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:barradenavegacion.tpl' => 1,
    'file:piedepagina.tpl' => 1,
  ),
),false)) {
function content_5f046a6a19adb2_36163827 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:barradenavegacion.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="row contenedorficha ">
  <table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">usuario</th>
        <th scope="col">Permisos de administrador</th>
        <th scope="col">Baneo</th>
      </tr>
    </thead>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['usuarios']->value, 'usuario');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['usuario']->value) {
?>
      <tbody>
        <tr>
          <th scope="row">
            <p><?php echo $_smarty_tpl->tpl_vars['usuario']->value->username;?>
</p>
          </th>
          <td>
           <?php if (($_smarty_tpl->tpl_vars['usuario']->value->privilegio == 2)) {?>
              <a type="button" class=" btn btn-warning " href="permiso/<?php echo $_smarty_tpl->tpl_vars['usuario']->value->privilegio;?>
/<?php echo $_smarty_tpl->tpl_vars['usuario']->value->id_usuario;?>
">
              <i class="fas fa-user fa-2x"></i> 
              Quitar permisos
              </a>
              <?php } else { ?>
                <a type="button" class="btn btn-primary" href="permiso/<?php echo $_smarty_tpl->tpl_vars['usuario']->value->privilegio;?>
/<?php echo $_smarty_tpl->tpl_vars['usuario']->value->id_usuario;?>
">
                  <i class="fa fa-diamond fa-2x" aria-hidden="true"></i>
                  Dar permisos
                </a>
           <?php }?>
          </td>
          <td> 
            <a type="button" href="expulsar/<?php echo $_smarty_tpl->tpl_vars['usuario']->value->id_usuario;?>
" class="btn btn-danger">Baneo Permanente</a>
          </td>
        </tr>
      </tbody>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
  </table>
</div>


<?php $_smarty_tpl->_subTemplateRender('file:piedepagina.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
