<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-07 18:07:35
  from 'C:\xampp\htdocs\servidor\tpe2w2\templates\vue\comentarios.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f049dc71d0198_44398248',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dfaad25285a8fcf81c5eba2b28130b2edcb95186' => 
    array (
      0 => 'C:\\xampp\\htdocs\\servidor\\tpe2w2\\templates\\vue\\comentarios.tpl',
      1 => 1594137985,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f049dc71d0198_44398248 (Smarty_Internal_Template $_smarty_tpl) {
?>

<section id="app-ficha">

    <div v-if="permiso>= 1" class="card">
              
        <div v-if="loading">Cargando...</div>
        
        <div v-if="!loading" v-for="comentario in comentarios" class="overflow-auto bg-secondary col-md-12 mt-3 pb-3 ">
            <div class="row col-md-12 justify-content-between align-items-center  py-2 " >
                <div class= "row  align-items-center px-4"> 
                    <h4>{{comentario.username}}</h4>
                    <div v-if="comentario.voto>=3"type="button" class="btn btn-success mx-1">+{{comentario.voto}}</div>
               
                    <div v-else type="button" class="btn btn-danger mx-1">+{{comentario.voto}}</div>
                    
                </div>
                <button v-if="permiso>1" v-on:click="eliminarComentario(comentario.comentario_id)" type="button" class="btn btn-danger" id="enviar"  ><i class="fas fa-trash"></i></button>
               
            </div>
            <div class="bg-light border border-primary rounded-lg col-md-11">
                <p>{{comentario.comentario}}
                </p>
            </div>        
        </div>
         
        

    </div>

</section>

<?php }
}
