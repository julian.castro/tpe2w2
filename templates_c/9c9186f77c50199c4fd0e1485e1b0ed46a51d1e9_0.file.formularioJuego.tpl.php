<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-06 23:50:24
  from 'C:\xampp\htdocs\servidor\tpe2w2\templates\formularioJuego.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f039ca0871294_24254861',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9c9186f77c50199c4fd0e1485e1b0ed46a51d1e9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\servidor\\tpe2w2\\templates\\formularioJuego.tpl',
      1 => 1594072149,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:barradenavegacion.tpl' => 1,
  ),
),false)) {
function content_5f039ca0871294_24254861 (Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php $_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
  <?php $_smarty_tpl->_subTemplateRender('file:barradenavegacion.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
  <div class="ingresarjuego">
    <form action="agregar" method="POST" enctype="multipart/form-data">
    <?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
      <div class="alert alert-danger"><?php echo $_smarty_tpl->tpl_vars['mensaje']->value;?>
</div>
    <?php }?>
      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label for="validationDefault01" class="textoform">Titulo</label>
          <input type="text" class="form-control" id="validationDefault01" name="titulo" required>
        </div>
        <div class="col-md-6 mb-3">
          <label for="validationDefault03" class="textoform">Imagen del Juego</label>
          <input  type="file" name="imput"  id="imageToUpload" class="form-control">
        </div>
      </div>
      <div class="form-row">
        <div class="col-md-12 mb-3">
          <label for="validationDefault03" class="textoform">Descripcion del juego</label>
          <input type="text" class="form-control" id="validationDefault03"  name="descripcion"  required>
        </div>
      </div>

      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label for="validationDefault03" class="textoform">Sistema Operativo</label>
          <input type="text" class="form-control" id="validationDefault03"  name="so"  required>
        </div>
    
        <div class="col-md-6 mb-3">
          <label for="validationDefault03" class="textoform">Video</label>
          <input type="text" class="form-control" id="validationDefault03"  name="grafico"  required>
        </div>
      </div>

      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label for="validationDefault03" class="textoform">Espacio en disco</label>
          <input type="text" class="form-control" id="validationDefault03"  name="espacio"  required>
        </div>

         
        <div class="col-md-6 mb-3">
          <label for="validationDefault04" class="textoform">Categoria</label>
            <select class="custom-select" name= 'categoria' required>
              <option selected disabled value="" >Elegir...</option>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categorias']->value, 'categoria');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['categoria']->value) {
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['categoria']->value->id_categoria;?>
"> <?php echo $_smarty_tpl->tpl_vars['categoria']->value->titulo;?>
 </option>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </select>
        </div> 
        

      </div>     

      <div class="form-row">
        <div class="col-md-12 mb-3">
          <label for="validationDefault02" class="textoform">Link de descarga</label>
          <input type="text" class="form-control" id="validationDefault02" name="link" required>
        </div>
      </div>    
      <button class="btn btn-primary" type="submit">Cargar juego</button>
    </form>
  </div><?php }
}
