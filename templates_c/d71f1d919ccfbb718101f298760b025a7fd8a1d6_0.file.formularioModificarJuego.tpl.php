<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-06 23:21:44
  from 'C:\xampp\htdocs\servidor\tpe2w2\templates\formularioModificarJuego.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f0395e8110566_08141471',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd71f1d919ccfbb718101f298760b025a7fd8a1d6' => 
    array (
      0 => 'C:\\xampp\\htdocs\\servidor\\tpe2w2\\templates\\formularioModificarJuego.tpl',
      1 => 1594070495,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:barradenavegacion.tpl' => 1,
  ),
),false)) {
function content_5f0395e8110566_08141471 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="contenedorform">

  <?php $_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
  <?php $_smarty_tpl->_subTemplateRender('file:barradenavegacion.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
  <div class="ingresarjuego">
    <form action="confirmarcambiosjuegos/<?php echo $_smarty_tpl->tpl_vars['juego']->value->id_ficha;?>
" method="POST" enctype="multipart/form-data">
    <?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
      <div class="alert alert-danger"><?php echo $_smarty_tpl->tpl_vars['mensaje']->value;?>
</div>
    <?php }?>
      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label for="validationDefault01" class="textoform">Titulo</label>
          <input type="text" class="form-control" id="validationDefault01" name="titulo" value="<?php echo $_smarty_tpl->tpl_vars['juego']->value->titulo;?>
">
        </div>
        <div class="col-md-6 mb-3">
          <label for="validationDefault03" class="textoform">Imagen del Juego</label>
          <input  type="file" name="imput"  id="imageToUpload" class="form-control">

        </div>
      </div>
      <div class="form-row">
        <div class="col-md-12 mb-3">
          <label for="validationDefault03" class="textoform">Descripcion del juego</label>
          <input type="text" class="form-control" id="validationDefault03"  name="descripcion"  value="<?php echo $_smarty_tpl->tpl_vars['juego']->value->descripcion;?>
">
        </div>
      </div>

      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label for="validationDefault03" class="textoform">Sistema Operativo</label>
          <input type="text" class="form-control" id="validationDefault03"  name="so" value="<?php echo $_smarty_tpl->tpl_vars['juego']->value->so;?>
">
        </div>
    
        <div class="col-md-6 mb-3">
          <label for="validationDefault03" class="textoform">Video</label>
          <input type="text" class="form-control" id="validationDefault03"  name="grafico" value="<?php echo $_smarty_tpl->tpl_vars['juego']->value->graficos;?>
">
        </div>
      </div>

      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label for="validationDefault03" class="textoform">Espacio en disco</label>
          <input type="text" class="form-control" id="validationDefault03"  name="espacio" value="<?php echo $_smarty_tpl->tpl_vars['juego']->value->espacio;?>
">
        </div>

         
        <div class="col-md-4 mb-3">
          <label for="validationDefault04" class="textoform">Categoria</label>
            <select class="custom-select" name= 'categoria' required>
              <option selected disabled value="" >Elegir...</option>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categorias']->value, 'categoria');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['categoria']->value) {
?>
                    <option 
                    <?php if (($_smarty_tpl->tpl_vars['juego']->value->id_categoria == $_smarty_tpl->tpl_vars['categoria']->value->id_categoria)) {?>
                      selected
                    <?php }?>
                    value="<?php echo $_smarty_tpl->tpl_vars['categoria']->value->id_categoria;?>
"> <?php echo $_smarty_tpl->tpl_vars['categoria']->value->titulo;?>
 </option>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </select>
        </div>
      </div>
      <div class="form-row">
        <div class="col-md-12 mb-3">
          <label for="validationDefault02" class="textoform">Link de descarga</label>
          <input type="text" class="form-control" id="validationDefault02" name="link" value="<?php echo $_smarty_tpl->tpl_vars['juego']->value->link;?>
">
        </div>
      </div>
      <button class="btn btn-primary" type="submit">Confirmar cambios</button>
    </form>
  </div>

</div><?php }
}
