<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-07 00:31:20
  from 'C:\xampp\htdocs\servidor\tpe2w2\templates\formularioRegistro.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f03a638aa7582_70129119',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '051d22c1995f7966849e1ec0a9e297f10675a7b3' => 
    array (
      0 => 'C:\\xampp\\htdocs\\servidor\\tpe2w2\\templates\\formularioRegistro.tpl',
      1 => 1594070699,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:barradenavegacion.tpl' => 1,
  ),
),false)) {
function content_5f03a638aa7582_70129119 (Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php $_smarty_tpl->_subTemplateRender('file:barradenavegacion.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <div class="formadministrador">
                <form action="registro" method="Post">
                    
                    <?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
                    <div class="alert alert-danger"><?php echo $_smarty_tpl->tpl_vars['mensaje']->value;?>
</div>
                    <?php }?>


                    <div class="form-group">
                        <label for="validationDefault02" class="textoform">Email </label>
                        <input type="email" class="form-control" name="email"   aria-describedby="emailHelp" required>
                    </div> 

                    <div class="form-group">
                        <label for="validationDefault02" class="textoform">Contraseña</label>
                        <input type="password" name="contraseña" class="form-control" required >
                    </div>
                    
                    <div class="form-group">
                        <label for="validationDefault02" class="textoform">nombre de usuario</label>
                        <input type="text" class="form-control" id="validationDefault02" name="nombre" required>
                    </div>

                    <button type="submit" class="btn btn-primary" >Registrar</button>
                </form>
        </div>
<?php }
}
