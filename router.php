<?php
    define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');

    
    require_once 'libs/router/Router.php';

    require_once 'controllers/ficha.controller.php';

    require_once 'controllers/categoria.controller.php';

    require_once 'controllers/formulario.controller.php';
    
    require_once 'controllers/autentificador.controller.php';

    $router = new Router();
    
    $action= $_REQUEST['action'];
    $urlAction= explode('/',$action);

    if($urlAction[0]==''){
        $urlAction[0]='home';
    }

//router20/fichacontroller/33/fichamodel43
    switch ($urlAction[0]){
        case 'home': {
           $ctrl= new ControladorFicha();
           $ctrl->listarFichas();
        } break;
//router26/----------------------------------------------------------------
        case 'listadojuegos': {
            $ctrl= new ControladorFicha();
            $juego=$urlAction[1];
            $ctrl ->listadoJuegos( $juego);
        } break;
//----------------------------------------------------------------
        case 'fichajuegos':{
            $ctrl= new ControladorFicha();
            $juego=$urlAction[1];
            $ctrl ->mostrarJuego($juego);
        }break;
//------------------------------------------------------------------
        case 'cargarjuego':{
            $ctrl= new ControladorFormulario();
            $ctrl->formularioCargarJuego();
        }break;
        case 'agregar':{
            $ctrl= new ControladorFicha();
            $ctrl->subirFicha();
        }break;
//----------------------------------------------------------------------
        case 'modificarjuego':{
            $ctrl= new ControladorFormulario();
            $juego=$urlAction[1];
            $ctrl->formularioModificarJuego($juego);
        }break;
        case 'confirmarcambiosjuegos':{
            $ctrl= new ControladorFicha();
            $juego=$urlAction[1];
            $ctrl->confirmarcambiosjuegos($juego);
        }break;
//-------------------------------------------------------------------
        case 'eliminarjuego':{
            $ctrl= new ControladorFicha();
            $juego=$urlAction[1];
            $ctrl->eliminarJuego($juego);
        }break;
//-------------------------------------------------------------------
        case 'ingresar':{
            $ctrl= new ControladorFormulario();
            $ctrl->formularioIngresar();
        }break;
        case 'registrar':{
            $ctrl= new ControladorFormulario();
            $ctrl->formularioRegistro();
        }break;
        case 'registro':{
            $ctrl= new ControladorAutentificador();
            $ctrl->registro();
        }break;
        case 'verificar':{
            $ctrl= new ControladorAutentificador();
            $ctrl->verificar();
        }break;
        case 'cerrarsesion':{
            $ctrl= new ControladorAutentificador();
            $ctrl->cerrarSesion();
        }break;
        case 'usuarios':{
            $ctrl= new ControladorAutentificador();
            $ctrl->listarUsuarios();
        }break;
        case 'permiso':{
            $ctrl= new ControladorAutentificador();
            $ctrl->administrarPermisos($urlAction[1], $urlAction[2]);
        }break;
        case 'expulsar':{
            $ctrl= new ControladorAutentificador();
            $ctrl->eliminarUsuario($urlAction[1]);
        }break;
        
//-------------------------------------------------------------------
        case 'cargarcategoria':{
            $ctrl= new ControladorFormulario();
            $ctrl->formularioCargarCategoria();
        }break;
        case 'insertarcategoria':{
            $ctrl= new ControladorCategoria();
            $ctrl->subirCategoria();
        }break;
        case 'modificarcategoria':{
            $id_categoria= $urlAction[1];
            $ctrl= new ControladorFormulario();
            $ctrl->formularioModificarCategoria($id_categoria);
        }break;
        case 'listadoCategorias': {
            $ctrl= new ControladorCategoria();
            $categoria=$urlAction[1];
            $ctrl ->listadoCategorias( $categoria);
        } break;
//router94/categoriacontroller34/categoriamodelo30-------------------------
        case 'eliminarcategoria':{
            $ctrl= new ControladorCategoria();
            $categoria=$urlAction[1];
            
            $ctrl->eliminarCategoria($categoria);
        }break;
        case 'confirmarcambioscategoria':{
            $ctrl= new ControladorCategoria();
            $categoria=$urlAction[1];
            $ctrl->confirmarCambiosCategoria($categoria);
        }break;
      
    }