<?php
require_once 'models/comentario.model.php';
require_once 'api/api.view.php';

class APIControladorComentarios {


    private $model;
    private $view;
    private $data;

    public function __construct() {
        $this->model =  new ModeloComentarios();
        $this->view = new APIVista();
        
        $this->data = file_get_contents("php://input");
    }


    public function traerComentarios($params = []){
        $juego = $params[':ID'];
        $comentarios= $this->model->traerComentarios($juego);
        $this->view->response($comentarios, 200);
    }
    public function insertarComentario(){
        $body = $this->getData();

        $juego = $body->juego;
        $usuario = $body->id_usuario;
        $comentario = $body->texto;
        $voto=  $body->puntaje;

        $this->model->insertarComentario( $usuario,$juego, $comentario, $voto);
        //$this->view->response($comentarios, 200);
    }
    public function eliminarComentario($params = []) {
      
        $response=$this->model->eliminarComentario($params[':ID']);
        if ($response){
             $this->view->response($response, 200);
        } else {
            $response="error en el borrado";
            $this->view->response($response, 500);
        }
        
       
    }
    function getData(){
        return json_decode($this->data); 
    }

}